<?php

require('animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("Shaun");

echo "<h4>Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("Buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump(); // "hop hop"

$sungokong = new Ape("Kera Sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "Auooo"



?>